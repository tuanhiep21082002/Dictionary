
import java.io.*;
import java.util.Scanner;

public class DictionaryApplication {

    static Dictionary dictionary = new Dictionary();
    static DictionaryManagement manager = new DictionaryManagement(dictionary);
    static DictionaryCommandLine command = new DictionaryCommandLine(manager);

    static void runApplication() {
        System.out.println("        ---DICTIONARY---");
        System.out.println("1. Show all word");
        System.out.println("2. Look up");
        System.out.println("3. Add word");
        System.out.println("4. Remove word");
        System.out.println("5. Exit");
        System.out.println();
    }

    public static void main(String[] args) {
        manager.insertFromFile();
        boolean isRunning = true;
        Scanner scanner = new Scanner(System.in);
        while (isRunning) {
            runApplication();
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    command.showAllWords();
                    break;
                case 2:
                    manager.dictionaryLookup();
                    break;
                case 3:
                    manager.addWord();
                    break;
                case 4:
                    manager.removeWord();
                    break;
                case 5:
                    isRunning = false;
                    break;
            }
        }
        manager.dictionaryExportToFile();
        scanner.close();
    }

}

