import java.util.*;
import java.io.*;

public class DictionaryManagement {

    public Dictionary dictionary = null;
    final String url = "dictionaries.txt";

    public DictionaryManagement (Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public void insertFromCommandline() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Insert the amount of word: ");
        int wordAmount = scanner.nextInt();
        System.out.println();
        scanner.nextLine();

        for (int i = 1; i <= wordAmount; i++) {
            System.out.print("Insert " + i + " target : ");
            String target = scanner.nextLine();
            System.out.print("Insert " + i + " explain: ");
            String explain = scanner.nextLine();
            dictionary.addWord(target, explain);
        }
        System.out.println();
    }

    //Load words from file
    public void insertFromFile() {
        try {
            System.out.println("Insert from file ...");
            File file = new File(url);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String[] data = scanner.nextLine().split("\\|");
                dictionary.addWord(data[0].trim(), data[1].trim());
            }
            scanner.close();
            System.out.println("Insert done.");
        } catch (FileNotFoundException e) {
            System.out.println("Can't read file: " + url);
        }
    }

    public void dictionaryLookup() {
        System.out.println("Look up:");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Insert the word you want to find: ");
        String target = scanner.nextLine();
        if (dictionary.word.containsKey(target)) {
            System.out.println("Meaning: " + dictionary.word.get(target).word_explain);
        } else {
            System.out.println("Your word is not in dictionary yet");
        }
        System.out.println();
    }

    public void addWord() {
        System.out.println("Add Word");
        System.out.print("Insert word target: ");
        Scanner scanner = new Scanner(System.in);
        String target = scanner.nextLine();
        if (dictionary.word.containsKey(target)) {
            System.out.println("Word '" + target + "' has already existed with meaning '" + dictionary.word.get(target).word_explain + "'");
            System.out.print("Input 'y' to continue to change the meaning: ");
            String temp = scanner.nextLine();
            if (temp.equals("y") || temp.equals("Y")) {
                System.out.print("Insert word explain: ");
                String explain = scanner.nextLine();
                dictionary.word.get(target).word_explain = explain;
            }
        } else {
            System.out.print("Insert word explain: ");
            String explain = scanner.nextLine();
            dictionary.addWord(target, explain);
        }
        System.out.println();
    }

    public void removeWord() {
        System.out.print("Insert word you want to remove: ");
        Scanner scanner = new Scanner(System.in);
        String target = scanner.nextLine();
        if (dictionary.wordList.remove(target)) {
            dictionary.word.remove(target);
            System.out.println("Remove word " + target + "successfully");
        } else {
            System.out.println("Dictionary doesn't contain the word " + target);
        }
        System.out.println();
    }

    public void dictionaryExportToFile() {
        try {
            System.out.println("Start writing");
            FileWriter writer = new FileWriter(url);
            for (String target : dictionary.wordList) {
                writer.write(target + " | " + dictionary.word.get(target).word_explain + "\n");
            }
            writer.close();
            System.out.println(" Writing completed");
        } catch (IOException e) {
            System.out.println("Can't write to file");
        }
        System.out.println();
    }
}
